// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
// jh: 부트스트랩 제외('=' 제외했음)
//  require bootstrap-sprockets
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(document).ready(function() {

  // jh : 상단으로가기 레이어 클릭시
   $( window ).scroll( function() {
     if ( $( this ).scrollTop() > 200 ) {
       $( '.top' ).fadeIn();
     } else {
       $( '.top' ).fadeOut();
     }
   });
   $( '.top' ).click( function() {
     $( 'html, body' ).animate( { scrollTop : 0 }, 400 );
     return false;
   });

  //  // 페이스북 js다운
  //  (function(d, s, id) {
  //    var js, fjs = d.getElementsByTagName(s)[0];
  //    if (d.getElementById(id)) return;
  //    js = d.createElement(s); js.id = id;
  //    js.src = "//connect.facebook.net/ko_KR/sdk.js#xfbml=1&version=v2.8";
  //    fjs.parentNode.insertBefore(js, fjs);
  //  }(document, 'script', 'facebook-jssdk'));
});

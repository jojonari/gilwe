class SessionController < ApplicationController
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id
    session[:user_uid] = user.uid

    redirect_to root_url
  end

  def destroy
    session[:user_id] = nil
    session[:user_uid] = nil
    redirect_to root_url
  end
end

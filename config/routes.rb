Rails.application.routes.draw do


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    root 'main#main'

    match 'auth/:provider/callback', to: 'session#create', via: [:get, :post]
    match 'auth/failure', to: redirect('/'), via: [:get, :post]
    match 'signout', to: 'session#destroy', as: 'signout', via: [:get, :post]
    match 'main1', to: 'main1#main1', as: 'main0', via: [:get, :post]
    match 'roads', to: 'road#roads', as: 'roads', via: [:get, :post]
    match 'road', to: 'road#index', as: 'road', via: [:get, :post]

end

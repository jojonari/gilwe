//*********************
//메인페이지 이미지 롤링
//*********************
function fn_rollToEx(containerID, slideID){

	// 롤링할 객체를 변수에 담아둔다.
	var el = $('#'+containerID).find('#'+slideID);
	var lastChild;
	var speed = 3000;
	var timer = 0;

	el.data('prev', $('#'+containerID).find('.prev'));	//이전버튼을 data()메서드를 사용하여 저장한다.
	el.data('next', $('#'+containerID).find('.next'));	//다음버튼을 data()메서드를 사용하여 저장한다.
	el.data('size', el.children().outerWidth());		//롤링객체의 자식요소의 넓이를 저장한다.
	el.data('len', el.children().length);				//롤링객체의 전체요소 개수
	el.data('animating',false);

	el.css('width',el.data('size')*el.data('len'));		//롤링객체의 전체넓이 지정한다.

	//el에 첨부된 prev 데이타를 클릭이벤트에 바인드한다.
	el.data('prev').bind({
		click:function(e){
			e.preventDefault();
			movePrevSlide();
		}
	});

	//el에 첨부된 next 데이타를 클릭이벤트에 바인드한다.
	el.data('next').bind({
		click:function(e){
			e.preventDefault();
			moveNextSlide();
		}
	});

	function movePrevSlide(){
		if(!el.data('animating')){
			//롤링객체의 끝에서 요소를 선택하여 복사한후 변수에 저장한다.
			var lastItem = el.children().eq(-2).nextAll().clone(true);
			lastItem.prependTo(el);		//복사된 요소를 롤링객체의 앞에 붙여놓는다.
			el.children().eq(-2).nextAll().remove();	//선택된 요소는 끝에서 제거한다
			el.css('left','-'+(el.data('size')*1+'px'));	//롤링객체의 left위치값을 재설정한다.

			el.data('animating',true);	//애니메이션 중복을 막기 위해 첨부된 animating 데이타를 true로 설정한다.

			el.animate({'left': '0px'},'normal',function(){		//롤링객체를 left:0만큼 애니메이션 시킨다.
				el.data('animating',false);
			});
		}
		return false;
	}

	function moveNextSlide(){
		if(!el.data('animating')){
			el.data('animating',true);

			el.animate({'left':'-'+(el.data('size')*1)+'px'},'normal',function(){	//롤링객체를 애니메이션 시킨다.
				//롤링객체의 앞에서 요소를 선택하여 복사한후 변수에 저장한다.
				var firstChild = el.children().filter(':lt('+1+')').clone(true);
				firstChild.appendTo(el);	//복사된 요소를 롤링객체의 끝에 붙여놓는다.
				el.children().filter(':lt('+1+')').remove();	//선택된 요소를 앞에서 제거한다
				el.css('left','0px');	////롤링객체의 left위치값을 재설정한다.

				el.data('animating',false);
			});
		}
		return false;
	}
  setInterval(function(){ moveNextSlide() }, 3000);

}

//*********************
//메인페이지 LIST
//*********************

  //  fn_draw_line(fn_draw_map('map1', locations[0].lat, locations[0].lng), path);
  //       url:'http://gilwe.com:3000/roads.json?accesstoken=EAAaiKOWIPFkBAFdyZBNtJWXZA1Ww0m3MLMiJ992OX0PTM4C3wZCdkLYEOJoI98gnnMIzmrVdCdYxipH73OyKpqRyAprZBg5XbmbGT3Uy4k3XPeYA2TMKZBI3rFFfqs5WKj73Wxlp06Y4Mtpq9FSkdBs4kZAi8CmT4ZD',

var j=0;
function fn_listData(){
  $.ajax({
       url:'http://gilwe.com:3000/roads.json',
       dataType:'json',
         type: 'GET',
      beforeSend: function (request)
            {
                request.setRequestHeader("keyType", "facebook");
                request.setRequestHeader("accessToken", "EAAaiKOWIPFkBAFdyZBNtJWXZA1Ww0m3MLMiJ992OX0PTM4C3wZCdkLYEOJoI98gnnMIzmrVdCdYxipH73OyKpqRyAprZBg5XbmbGT3Uy4k3XPeYA2TMKZBI3rFFfqs5WKj73Wxlp06Y4Mtpq9FSkdBs4kZAi8CmT4ZD");
            },
       success:function(data){
         if(data != null){
           var recommends = data.recommends;
           for(var i =0; i < 3; i++){
             if(j < 0) j=0;
             else if(j>=recommends.length)j=0;
               console.log('idx:' + j + ' : '+recommends.length);
               $('.list_img').eq(i).attr('src',recommends[j].image);
               $('.list_name').eq(i).html(recommends[j].title);
               $('.list_info').eq(i).html(recommends[j].text);
               $('.userImg').eq(i).attr('src',recommends[i].image);
               j++;
             }
         }
       }
   })
}
//스크롤이미지 시작
fn_rollToEx('slider', 'photo');
//리스트 데이터
// fn_listData();
// function fn_prev(){
//   j -= 6;
//   fn_listData();
// };
//
// function fn_next(){
//   fn_listData();
// };\

function sendEmail(){
  alert(
    '사전등록이 완료되었습니다.'
  );
}

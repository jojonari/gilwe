
// 지도를 생성한다
function fn_draw_map(selector, map_x, map_y){
	var mapContainer = document.getElementById(selector), // 지도를 표시할 div
		    mapOption = {
		        center: new daum.maps.LatLng(map_x, map_y), // 지도의 중심좌표
		        level: 6, // 지도의 확대 레벨
		        mapTypeId : daum.maps.MapTypeId.ROADMAP // 지도종류
						,draggable : true
						,scrollwheel : true
						,disableDoubleClick : true
						,disableDoubleClickZoom : true
						,keyboardShortcuts : true
		    };
		return new daum.maps.Map(mapContainer, mapOption);
}
	// 지도에 선을 표시한다
function fn_draw_line(map, path){
	map.setDraggable(true);
	var polyline = new daum.maps.Polyline({
		map: map, // 선을 표시할 지도 객체
		path: path,
		strokeWeight: 3, // 선의 두께
		strokeColor: '#FF0000', // 선 색
		strokeOpacity: 0.9, // 선 투명도
		strokeStyle: 'solid' // 선 스타일
	});
	startMarker(map,path[0]);
	arriveMarker(map,path[path.length-1]);

}

// 시작 마커
function startMarker(map, start_position){
	var startSrc = 'http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/red_b.png', // 출발 마커이미지의 주소입니다
	    startSize = new daum.maps.Size(50, 45), // 출발 마커이미지의 크기입니다
	    startOption = {offset: new daum.maps.Point(15, 43)}; // 출발 마커이미지에서 마커의 좌표에 일치시킬 좌표를 설정합니다 (기본값은 이미지의 가운데 아래입니다)
			// 출발 마커 이미지를 생성합니다
			var startImage = new daum.maps.MarkerImage(startSrc, startSize, startOption);
	// 출발 마커를 생성합니다
	var startMarker = new daum.maps.Marker({
	    map: map, // 출발 마커가 지도 위에 표시되도록 설정합니다
	    position: start_position,
	    draggable: true, // 출발 마커가 드래그 가능하도록 설정합니다
	    image: startImage // 출발 마커이미지를 설정합니다
	});
	// 출발 마커에 dragend 이벤트를 등록합니다
	daum.maps.event.addListener(startMarker, 'dragend', function() {
	     // 출발 마커의 드래그가 종료될 때 마커 이미지를 원래 이미지로 변경합니다
	    startMarker.setImage(startImage);
	});
}

// 도착 마커
function arriveMarker(map, arrive_position){
	var arriveSrc = 'http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/blue_b.png', // 도착 마커이미지 주소입니다
	arriveSize = new daum.maps.Size(50, 45), // 도착 마커이미지의 크기입니다
	arriveOption = {offset: new daum.maps.Point(15, 43)}; // 도착 마커이미지에서 마커의 좌표에 일치시킬 좌표를 설정합니다 (기본값은 이미지의 가운데 아래입니다)
	// 도착 마커 이미지를 생성합니다
	var arriveImage = new daum.maps.MarkerImage(arriveSrc, arriveSize, arriveOption);
	// 도착 마커를 생성합니다
	var arriveMarker = new daum.maps.Marker({
	    map: map, // 도착 마커가 지도 위에 표시되도록 설정합니다
	    position: arrive_position,
	    draggable: true, // 도착 마커가 드래그 가능하도록 설정합니다
	    image: arriveImage // 도착 마커이미지를 설정합니다
	});
	// 도착 마커에 dragend 이벤트를 등록합니다
	daum.maps.event.addListener(arriveMarker, 'dragend', function() {
	     // 도착 마커의 드래그가 종료될 때 마커 이미지를 원래 이미지로 변경합니다
	    arriveMarker.setImage(arriveImage);
	});
}




// var path1 = [ // 선을 구성하는 좌표 배열
// 	new daum.maps.LatLng(37.56682, 126.97661),
// 	new daum.maps.LatLng(37.56782, 126.97762),
// 	new daum.maps.LatLng(37.56783, 126.97763),
// 	new daum.maps.LatLng(37.56784, 126.97764),
// 	new daum.maps.LatLng(37.56785, 126.97765),
// 	new daum.maps.LatLng(37.56786, 126.97766),
// 	new daum.maps.LatLng(37.56787, 126.97766),
// 	new daum.maps.LatLng(37.56782, 126.97667)
// ];
// var path2 = [ // 선을 구성하는 좌표 배열
// 	new daum.maps.LatLng(37.56682, 126.97665),
// 	new daum.maps.LatLng(37.56782, 126.97765),
// 	new daum.maps.LatLng(37.56783, 126.97765),
// 	new daum.maps.LatLng(32.56784, 122.97761),
// 	new daum.maps.LatLng(37.56785, 126.97762),
// 	new daum.maps.LatLng(37.56786, 126.97763),
// 	new daum.maps.LatLng(37.56787, 126.97764),
// 	new daum.maps.LatLng(37.56782, 126.97665)
// ];
// var path3 = [ // 선을 구성하는 좌표 배열
// 	new daum.maps.LatLng(37.56681, 126.97663),
// 	new daum.maps.LatLng(37.56781, 126.97761),
// 	new daum.maps.LatLng(37.56781, 126.97766),
// 	new daum.maps.LatLng(37.56781, 126.97755),
// 	new daum.maps.LatLng(37.56781, 126.97744),
// 	new daum.maps.LatLng(37.56781, 126.97744),
// 	new daum.maps.LatLng(37.56711, 126.97733),
// 	new daum.maps.LatLng(37.56111, 126.97622)
// ];

 //fn_draw_line(fn_draw_map('map1', 37.56782, 126.97665), path1);
// fn_draw_line(fn_draw_map('map2', 37.56682, 126.97665), path2);
// fn_draw_line(fn_draw_map('map3', 37.56681, 126.97663), path3);
